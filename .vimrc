set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" Lightline statusbar
Plugin 'itchyny/lightline.vim'
" Color preview for CSS
Plugin 'ap/vim-css-color'
" Dracula colorscheme
Plugin 'dracula/vim', { 'name': 'dracula' }
" Nerdtree file system explorer
Plugin 'preservim/nerdtree'
" Icons
Plugin 'ryanoasis/vim-devicons'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

""""""""""""""""""""
" General Settings "
""""""""""""""""""""
set t_Co=256                    " Set if term supports 256 colors.
set number relativenumber       " Display line numbers
set laststatus=2                " Always show statusline
set noshowmode                  " Uncomment to prevent non-normal modes showing in powerline and below powerline
set encoding=utf8
set guifont=DroidSansMono\ Nerd\ Font\ 12
set autochdir

let g:rehash256 = 1

syntax on
colorscheme dracula
highlight Normal ctermbg=NONE

let g:lightline = {
      \ 'colorscheme': 'dracula',
      \ }

"""""""""""""""
" Text indent "
"""""""""""""""

set expandtab                   " Use spaces instead of tabs.
set smarttab                    " Be smart using tabs ;)
set shiftwidth=4                " One tab == four spaces.
set tabstop=4                   " One tab == four spaces.

"""""""""""
" Key Map "
"""""""""""
nnoremap <leader>n :NERDTreeFocus<CR>
"nnoremap <C-n> :NERDTree<CR>
nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

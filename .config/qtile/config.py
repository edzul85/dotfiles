# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess
from typing import List, Text  # noqa: F401

from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen, KeyChord, ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4" # Super key
alt = "mod1"

# Default program
terminal = "kitty"
launcher = "rofi"
email = "thunderbird"
fileman = "thunar"

##      KEY-BINDING     ##

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    ##Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    Key([mod], "space", lazy.layout.down(), desc="Switch window in stack layout"),
    
    # Switch between windows using arrow keys
    Key([mod], "Left", lazy.layout.left(), desc="Focus to left window"),
    Key([mod], "Right", lazy.layout.right(), desc="Focus to right window"),
    Key([mod], "Down", lazy.layout.down(), desc="Focus to top window"),
    Key([mod], "Up", lazy.layout.up(), desc="Focus to bottom window"),

    # Toggle floating window
    Key([mod, "shift"], "f", lazy.window.toggle_floating()),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Using arrow keys
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move focus window to left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move focus window to right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move focus window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move focus window up"),

    # Move to next or previous screen and group
    Key([mod],"period", lazy.next_screen(), desc="Move to next screen"),
    Key([mod],"comma", lazy.prev_screen(), desc="Move to previous screen"),
    Key([mod],"Page_Up", lazy.screen.next_group(), desc="Move to next group"),
    Key([mod],"Page_Down", lazy.screen.prev_group(), desc="Move to previous group"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"), 
    Key([mod], "g", lazy.layout.grow(), desc="Grow window"), 
    Key([mod], "s", lazy.layout.shrink(), desc="Shrink window"), 
    Key([mod], "m", lazy.layout.maximize(), desc="Maximize window"), 
    Key([mod, "shift"], "space", lazy.layout.flip(), desc="Flip window"), 

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Quit/Kill focused window"),
    
    # Qtile control
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Quit/Shutdown Qtile"),
    
    # Program key binding
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "e", lazy.spawn(email), desc="Launch email client"),
    Key([mod], "f", lazy.spawn(fileman), desc="Launch file manager"),
    Key([mod, "control"], "Return", lazy.spawn("betterlockscreen -l"), desc="Screenlock"),
    Key([mod], "x", lazy.spawn("archlinux-logout"), desc="Logout menu"),

    # Screenshot
    Key([], "Print", lazy.spawn("gnome-screenshot"), desc="Print screen entire desktop"),
    Key([mod], "Print", lazy.spawn("gnome-screenshot -i"), desc="Print screen selected area"),

    # Rofi launcher
    Key([mod], "a", lazy.spawn(launcher + " -show drun -show-icons"), desc="Launch rofi drun/app launcher"),
    Key([mod], "r", lazy.spawn(launcher + " -show run"), desc="Spawn a run command using rofi"),
    Key([alt], "Tab", lazy.spawn(launcher + " -show window -show-icons"), desc="Switch window using rofi"),

    ## XF86 Keyboard Keys ##
    # Explorer
    Key([], "XF86Explorer", lazy.spawn(fileman), desc="launch file manager"),

    # App search
    Key([], "XF86Search", lazy.spawn("xfce4-appfinder"), desc="launch application finder"),

    # Calculator
    Key([], "XF86Calculator", lazy.spawn("galculator"), desc="Launch calculator"),

    # Tools
    Key([], "XF86Tools", lazy.spawn("xfce4-settings-manager"), desc="launch setting manager"),

    # Player control
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous"), desc="Player control previous"),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next"), desc="Player control next"),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause"), desc="Player control toggle play-pause"),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop"), desc="Player control stop"),

    # Volume control

    Key([], "XF86AudioMute", lazy.spawn("amixer sset 'Master' toggle"), desc="toggle mute/unmute volume"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer sset 'Master' 2%-"), desc="decrease audio volume"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer sset 'Master' 2%+"), desc="increase audio volume"),

    ## Key Chords ##
    # Chat
    KeyChord([mod], "c", [
        Key([], "w", lazy.spawn("brave --app=https://web.whatsapp.com/"), desc="launch Whatsapp in Brave-browser app"),
        Key([], "t", lazy.spawn("telegram-desktop"), desc="launch telegram desktop")
    ]),

    # Terminal
    KeyChord([mod], "t", [
        Key([], "k", lazy.spawn("kitty"), desc="launch Kitty"),
        Key([], "a", lazy.spawn("alacritty"), desc="launch Alacritty")
    ]),

    # Browser
    KeyChord([mod], "b", [
        Key([], "b", lazy.spawn("brave"), desc="launch Brave-browser"),
        Key([], "i", lazy.spawn("brave -incognito"), desc="launch Incognito Brave-browser"),
        Key([], "c", lazy.spawn("google-chrome-stable"), desc="launch Google-chrome"),
        Key([], "f", lazy.spawn("firefox"), desc="launch firefox"),
        Key([], "q", lazy.spawn("qutebrowser"), desc="launch Qutebrowser"),
        Key([], "y", lazy.spawn("brave --app=https://youtube.com/"), desc="launch Youtube in Brave-browser app"),
        Key([], "n", lazy.spawn("brave --app=https://www.netflix.com/"), desc="launch Netflix in Brave-browser app"),
    ]),

    # Function Keys
    Key([mod], "F1", lazy.spawn("brave --app=http://docs.qtile.org/en/latest/"), desc="launch Qtile documentation in Brave-browser app"),
    Key([mod], "F2", lazy.spawn("code .config/qtile/"), desc="launch Qtile configuration using VSCode"),
    Key([mod, "shift"], "F2", lazy.spawn(terminal + " -e vim .config/qtile/config.py"), desc="launch Qtile configuration using vim"),
    Key([mod], "F3", lazy.spawn("archlinux-tweak-tool"), desc="launch archlinux-tweak-tool"),
    Key([mod], "F4", lazy.spawn("virt-manager"), desc="launch virtual manager"),
    Key([mod], "F5", lazy.spawn("lutris"), desc="launch lutris game manager"),
    Key([mod], "F6", lazy.spawn("easyeffects"), desc="launch easyeffects manager"),
    Key([mod], "F7", lazy.spawn("timeshift-launcher"), desc="launch timeshift backup/restore manager"),
]

##         GROUPS         ##

groups = [
    ## Browser
    Group(
        name="1", 
        matches=[Match(wm_class=["brave-browser", "google-chrome", "firefox", "youtube.com"])],
        layout="stack",
        label="" #"",""
    ),
    ## Work / Doc / Graphic Design
    Group(
        name="2", 
        matches=[Match(wm_class=["code", "docs.qtile.org__en_latest", "Xreader", "Zathura", "Gimp-2.10", "Inkscape" ])],
        layout="monadtall",
        label="" #"",""
    ),
    ## CLI
    Group(
        name="3", 
        matches=[Match(wm_class=["kitty"])], 
        layout="monadtall", 
        label="" #"",""
    ),
    ## Games
    Group(
        name="4", 
        matches=[Match(wm_class=["Steam", "ts4_x64.exe", "battle.net.exe", "pyrogenesis", "steam_app_238960", "sc2_x64.exe"])],
        layout="stack",
        label="" #"",""
    ),
    ## Virtual Machine
    Group(
        name="5",
        matches=[Match(wm_class=["Virt-manager", "cryptowatch_desktop"])],
        layout="stack",
        label="" #"",""
    ),
    ## Chat / Email
    Group(
        name="6",
        matches=[Match(wm_class=["web.whatsapp.com", "TelegramDesktop", "thunderbird"])],
        layout="monadtall",
        label="" #"",""
    ),
    ## Video & Music Player
    Group(
        name="7",
        matches=[Match(wm_class=["vlc", "www.netflix.com", "Spotify", "deadbeef", "nuclear"])],
        layout="stack",
        label="" #"",""
    ),
    ## Virtual Manager & Settings Manager
    Group(
        name="8", 
        matches=[Match(wm_class=["Xfce4-settings-manager", "Lutris", "easyeffects", "archlinux-tweak-tool.py"])],
        layout="monadtall",
        label="" #"",""
    ),
    ## Free
    #Group(
    #    name="9",
    #    matches=[Match(wm_class=[])],
    #    layout="stack",
    #    label="" #"",""
    #),
]

Groups = [Group(i) for i in "123456789"]
for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])

groups.append(ScratchPad('scratchpad', [
    DropDown('ncmpcpp', 'kitty -e ncmpcpp', height=0.7, width=0.70, y=0.017, x=0.287, opacity=1),
    DropDown('bashtop', 'kitty -e bashtop', height=0.972, width=0.70, y=0.012, x=0.15, opacity=1),
    DropDown('radio', 'qutebrowser radio.garden', height=0.7, width=0.30, y=0.017, x=0.687, opacity=1)
]))

keys.extend([
    Key([alt,"control"], "Delete", lazy.group['scratchpad'].dropdown_toggle('bashtop')),
])

# allow mod3+1 through mod3+0 to bind to groups; if you bind your groups
# by hand in your config, you don't need to do this.
#from libqtile.dgroups import simple_key_binder
#dgroups_key_binder = simple_key_binder("mod4")

##     COLORS   ##
# Color from Draculatheme
colors = [
                ["#282a36", "#282a36"],   # Background (black) 0
                ["#44475a", "#44475a"],   # Current Line (grey) 1
                ["#f8f8f2", "#f8f8f2"],   # Foreground (white) 2
                ["#6272a4", "#6272a4"],   # Comment (blue) 3
                ["#8be9fd", "#8be9fd"],   # Cyan 4
                ["#50fa7b", "#50fa7b"],   # Green 5
                ["#ff79c6", "#ff79c6"],   # Pink 6
                ["#bd93f9", "#bd93f9"],   # Purple 7
                ["#ff5555", "#ff5555"],   # Red 8
                ["#f1fa8c", "#f1fa8c"]    # Yellow 9
         ]  

##      LAYOUT        ##

layout_theme = {
    "border_width": 3,
    "margin": 11, 
    "border_focus": colors[7], 
    "border_normal": colors[6]
    }

layouts = [
    # layout.Max(**layout_theme),
    #layout.Columns(
    #    **layout_theme,
    #    border_on_single = True,
    #    border_focus_stack = colors[7],
    #    border_normal_stack = colors[7]
    #),  
    # Try more layouts by unleashing below layouts.
    layout.Stack(num_stacks=1, **layout_theme),
    # layout.Bsp(**layout_theme),
    # layout.Matrix(**layout_theme),
    layout.MonadTall(**layout_theme),
    # layout.MonadWide(**layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.Tile(**layout_theme),
    # layout.TreeTab(
    #     font="fira san bold",
    #     fontsize=12,
    #     border_width = 2,
    #     bg_color="282a36",
    #     fg_color="f8f8f2",
    #     active_bg = "bd93f9",
    #     active_fg = "50fa7b",
    #     inactive_bg = "44475a",
    #     inactive_fg = "f8f8f2",
    #     urgent_bg = "ff5555",
    #     urgent_fg = "f1fa8c",
    #     padding_left = 10,
    #     padding_x = 10,
    #     padding_y = 5,
    #     sections = ['Running'],
    #     section_padding = 0,
    #     section_top = 10,
    #     section_bottom = 20,
    #     level_shift = 8,
    #     vspace = 2,
    #     panel_width = 120
    # ),
    # layout.VerticalTile(),
    # layout.Zoomy(**layout_theme),
    # layout.Floating(**layout_theme),
]

##     MOUSE CALLBACKS   ##

def quit():
    qtile.cmd_spawn("archlinux-logout")
## 
## def topmem():
##     qtile.cmd_spawn("alacritty -e htop --sort-key=PERCENT_MEM")
## 
## def topcpu():
##     qtile.cmd_spawn("alacritty -e htop --sort-key=PERCENT_CPU")

##      WIDGETS      ##

def init_widgets_defaults():
    return dict(
        font = 'FiraCode NFM semibold',
        fontsize = 12,
        foreground = colors[2],
        fontshadow = None
    )

widget_defaults = init_widgets_defaults()

## Widgets for screen 1 ##

def init_widgets_list():
    widgets_list1 = [
        widget.Spacer(
            length = 16
        ),
        widget.CurrentLayoutIcon(
            custom_icon_paths = [os.path.expanduser("~/.config/qtile/layout-icons")],
            scale = 0.6,
            padding = 0
        ),
        widget.GroupBox(
            background = colors[0],
            fontsize = 20,
            margin_x = 2,
            margin_y = 4,
            padding_x = 2,
            padding_y = 0,
            active = colors[5],
            inactive = colors[1],
            rounded = False,
            highlight_method = "line",
            highlight_color = colors[0],
            this_screen_border = colors[7],
            this_current_screen_border = colors[7],
            other_screen_border = colors[6],
            other_current_screen_border = colors[6],
            urgent_alert_method = "line",
            urgent_border = colors[8],
            urgent_text = colors[8],
            borderwidth = 2,
            disable_drag = True,
        ),
        widget.Spacer(
            length = 5
        ),
        widget.TaskList(
            margin_x = 1,
            margin_y = 1,
            padding_x = 5,
            padding_y = 2,
            highlight_method = "block",
            border = colors[7],
            unfocused_border = colors[6],
            borderwidth = 2,
            urgent_alert_method = "block",
            urgent_border = colors[8],
            rounded = True,
            txt_floating = "",
            txt_minimized = " ",
            foreground = colors[0],
            icon_size = 14
        ),
        widget.Spacer(),
        widget.TextBox(
            text = "",
            fontsize = 20,
            foreground = colors[1],
            padding = 0
        ),
        widget.WidgetBox(
            widgets = [
                widget.TextBox(
                    text = " Radio ",
                    foreground = colors[7],
                    background = colors[1],
                    padding = 0,
                    mouse_callbacks = {'Button1': lazy.group['scratchpad'].dropdown_toggle('radio')}
                ),
                widget.Sep(
                    foreground = colors[3],
                    background = colors[1],
                    linewidth = 2
                ),
                widget.WidgetBox(
                    widgets = [
                        widget.Mpd2(
                            foreground = colors[7],
                            background = colors[1],
                            play_states = {'pause': '', 'play': '', 'stop': ''},
                            status_format = "{play_status} {title} by {artist}",
                            idle_format = "{play_status} {idle_message}",
                            idle_message = "MPD not playing",
                            max_chars = 70,
                            mouse_callbacks = {'Button2': lazy.group['scratchpad'].dropdown_toggle('ncmpcpp')}
                        ),
                    ],
                    text_closed = " Muic Player ",
                    text_open = " Muic Player =",
                    foreground = colors[7],
                    background = colors[1],
                ),
            ],
            text_closed = " Muic ",
            text_open = " Muic ",
            foreground = colors[7],
            background = colors[1],
        ),
        widget.TextBox(
            text = "",
            fontsize = 20,
            foreground = colors[1],
            padding = 0
        ),
        #widget.Sep(
        #    foreground = colors[3],
        #    linewidth = 2
        #),        
        widget.Clock(
            format = " %A",
            padding = 2,
            foreground = colors[7]
        ),
        widget.TextBox(
            text = "",
            fontsize = 20,
            foreground = colors[3],
            padding = 1
        ),
        widget.Clock(
            format = "%d-%m-%Y",
            padding = 2,
            foreground = colors[7]
        ),
        widget.TextBox(
            text = "",
            fontsize = 20,
            foreground = colors[3],
            padding = 1
        ),
        widget.Clock(
            format = "%I:%M %p ",
            padding = 2,
            foreground = colors[7]
        ),
        widget.WidgetBox(
            widgets = [
                widget.Systray(
                    padding = 5,
                    icon_size = 16,
                ),
                widget.Volume(
                    fmt = "Vol:{}",
                    padding = 5
                ),
            ],
            text_closed = "  ",
            text_open = "  ",
            fontsize = 8,
        ),
        widget.TextBox(
            text = "襤",
            foreground = colors[2],
            fontsize = 20,
            padding = 2,
            mouse_callbacks = {'Button1': quit}
        ),
        widget.Spacer(
            length = 16
        ),
    ]
    return widgets_list1

widgets_list = init_widgets_list()

##     SCREEN & BAR      ##

def init_widgets_screen():
    widgets_screen = widgets_list
    return widgets_screen

widgets_screen = init_widgets_screen()

def init_screens():
    return [
        Screen (
            top = bar.Bar(
                widgets = widgets_screen, 
                size = 22, 
                opacity = 1.0,
                margin = [0,0,0,0],
                background = colors[0]
            ),
        ),
    ]
    
screens = init_screens()

##  Drag floating layouts  ##

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = True
floating_layout = layout.Floating(**layout_theme, float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='Arcolinux-welcome-app.py'),
    Match(wm_class='Arcolinux-tweak-tool.py'),
    Match(wm_class='Arcolinux-calamares-tool.py'),
    Match(wm_class='confirm'),
    Match(wm_class='dialog'),
    Match(wm_class='download'),
    Match(wm_class='error'),
    Match(wm_class='file_progress'),
    Match(wm_class='notification'),
    Match(wm_class='splash'),
    Match(wm_class='toolbar'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='Galculator'),
    Match(wm_class='arcolinux-logout'),
    Match(wm_class='xfce4-terminal'),
    Match(wm_class='Nvidia-settings'),
    Match(wm_class='Alacritty'),
    Match(wm_class='Termite'),
    Match(wm_class='Thunar'),
    Match(wm_class='Pinentry-gtk-2'),
    ])
auto_fullscreen = True
focus_on_window_activation = "focus"    # "focus" or "smart" or "urgent"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])